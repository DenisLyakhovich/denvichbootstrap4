"use strict";

var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var	rename = require('gulp-rename');
var	notify = require('gulp-notify');
var	prefix = require('gulp-autoprefixer');
var livereload  = require('gulp-livereload');
var connect = require('gulp-connect');
var	minifyCSS = require('gulp-minify-css');
var wiredep = require('wiredep').stream;
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-clean-css');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
//var sftp = require('gulp-sftp');

//server connect
gulp.task('connect',  function(){
	connect.server({
		root: 'app',
		livereload: true
	})
})

//css
gulp.task('css', function () {
  	gulp.src('app/css/style.css')
   	.pipe(concatCss('bundle.css'))
   	.pipe(prefix('last 15 versions'))
   	.pipe(minifyCSS(''))
  	.pipe(rename('style.min.css'))
  	.pipe(gulp.dest('app/css'))
    .pipe(connect.reload())
    //.pipe(notify('Done, css changes!'))
})

//html
gulp.task('html', function(){
	gulp.src('app/index.html')
	.pipe(connect.reload())
	//.pipe(notify('Done, html changes!'))
})

//
gulp.task('images', function() {
    return gulp.src("app/images/*.+(jpg|jpeg|png|gif)")
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            interlaced: true
        }))
        .pipe(gulp.dest("dist/images"))
});

//bower
gulp.task('bower', function () {
	gulp.src('./app/index.html')
	.pipe(wiredep({
	directory : "app/bower_components"
}))
	.pipe(gulp.dest('./app'))
	//.pipe(notify('Done, link libryares!'))
});

//clean 
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

//bild
gulp.task('bild', ['clean'], function () {
    return gulp.src('app/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifycss()))
        .pipe(gulp.dest('dist'));
});

//sftp
//gulp.task('sftp', function () {
//    return gulp.src('dist/**/*')
//       .pipe(sftp({
//            host: 'denvich.com',
//            user: 'denvi12663_ftp',
//            pass: '198229lD',
//            remotePath: '/home/denvi12663/denvich.com/docs'
//        }));
//});

//watch
gulp.task('watch', function(){
	gulp.watch('app/css/*.css',['css'])
	gulp.watch('app/index.html', ['html'])
	gulp.watch('bower.json', ['bower'])
})

//default
gulp.task('default', ['connect', 'html', 'css', 'images', 'bower','clean', 'bild', 'watch'])
